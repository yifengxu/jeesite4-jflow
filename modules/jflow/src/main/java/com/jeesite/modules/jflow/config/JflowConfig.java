/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.jflow.config;

import java.util.Hashtable;

import javax.sql.DataSource;

import org.apache.shiro.subject.support.DefaultSubjectContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.jeesite.common.config.Global;

import BP.Difference.ContextHolderUtils;
import BP.Difference.Handler.BeanNameGenerator;
import BP.Sys.SystemConfig;


/**
 * JFlowConfig
 * @author ThinkGem
 * @version 2018年8月13日
 */
@Configuration
@ComponentScan(basePackages="BP.Difference", nameGenerator=BeanNameGenerator.class)
public class JflowConfig implements WebMvcConfigurer{
	
	@Bean
	public ContextHolderUtils jflowContextHolderUtils(DataSource dataSource){
		Hashtable<String, Object> prop = SystemConfig.getCS_AppSettings();
		prop.put("AppCenterDBType", Global.getProperty("jdbc.type"));
		prop.put("AppCenterDBDatabase", Global.getProperty("jdbc.schema"));
		prop.put("AppCenterDSN", Global.getProperty("jdbc.url"));
		prop.put("JflowUser", Global.getProperty("jdbc.username"));
		prop.put("JflowPassword", Global.getProperty("jdbc.password"));
		prop.put("JflowTestSql", Global.getProperty("jdbc.testSql"));
		ContextHolderUtils bean = new ContextHolderUtils();
		bean.setUserNoSessionKey(DefaultSubjectContext.PRINCIPALS_SESSION_KEY);
		bean.setDataSource(dataSource);
		return bean;
	}
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
//		String[] folders = new String[]{"WF","GPM","APP","SDKFlowDemo"};
//		for (String folder : folders){
//			registry.addResourceHandler("/"+folder+"/**")
//					.addResourceLocations("/jflow/"+folder+"/",
//							"classpath:/jflow/"+folder+"/")
//					.setCachePeriod(31536000); // 8小时
//		}
	}
	
}
